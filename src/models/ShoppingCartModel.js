// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo ShoppingCart Schema MongoDB
const ShoppingCartSchema = new Schema({
    Id: Schema.Types.ObjectId, // Trường Id có kiểu dữ liệu ObjectID
    Name: {
        type: String,
        required: true,
        unique: true
    },
    Type: {
        type: String,
        required: true,

    },
    ImageUrl: {
        type: String,
        required: true,
    },
    BuyPrice: {
        type: Number,
        required: true,

    },
    PromotionPrice: {
        type: Number,
        required: true,

    },
    Description: {
        type: String,
        required: false
    },
    Quantity: {
        type: Number,
        required: true,
        default: 100
    },
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
})

//Tạo ShoppingCart Model
const ShoppingCartModel = mongoose.model("ShoppingCart", ShoppingCartSchema);

//Export ShoppingCart Model
module.exports = { ShoppingCartModel };

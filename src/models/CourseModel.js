// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Course Schema MongoDB
const courseSchema = new Schema({
    _id: Schema.Types.ObjectId, // Trường _id có kiểu dữ liệu ObjectID
    FullName: {
        type: String,
        required: true,
        unique: true
    },
    PhoneNumber: {
        type: String,
        required: true,
        unique: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true,

    },
    Address: {
        type: String,
        required: false,
    },
    City: {
        type: String,
        required: false,
    },
    Country: {
        type: String,
        required: false
    },
    ShoppingCard: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Shopping'
        }
    ],
    WishList: [
        {
            type: Schema.Types.ObjectId,
            ref: 'WishList'
        }
    ],
    BuyAgain: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Review'
        }
    ],
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
})

//Tạo Course Model
const CourseModel = mongoose.model("Course", courseSchema);

//Export Course Model
module.exports = { CourseModel };

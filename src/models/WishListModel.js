// Import thư viện mongoose
const mongoose = require('mongoose');

// Phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo WishList Schema MongoDB 
const WishListSchema = new Schema({
    // Trường _id có kiểu dữ liệu ObjectID
    Id: Schema.Types.ObjectId,
    Name: {
        type: String,
        required: true,
        unique: true
    },
    Type: {
        type: String,
        required: true,

    },
    ImageUrl: {
        type: String,
        required: true,
    },
    BuyPrice: {
        type: Number,
        required: true,

    },
    PromotionPrice: {
        type: Number,
        required: true,

    },
    Description: {
        type: String,
        required: false
    },
    Quantity: {
        type: Number,
        required: true,
        default: 100
    },
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
});

// Tạo Model cho MongoDB từ Schema vừa khai báo
const WishListModel = mongoose.model('WishList', WishListSchema);

// Export model vừa tạo dưới dạng module
module.exports = { WishListModel };

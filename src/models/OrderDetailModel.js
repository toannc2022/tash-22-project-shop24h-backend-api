// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo OrderDetail Schema MongoDB
const OrderDetailSchema = new Schema({
    Id: Schema.Types.ObjectId, // Trường Id có kiểu dữ liệu ObjectID
    Order:
    {
        type: Schema.Types.ObjectId,
        ref: 'Order'
    }
    ,
    Product:
    {
        type: Schema.Types.ObjectId,
        ref: 'Product'
    }
    ,
    Quantity: {
        type: Number,
        required: true,
        default: 1
    },
    PriceEach: {
        type: Number,
        required: true,
        default: 1
    }

})

//Tạo OrderDetail Model
const OrderDetailModel = mongoose.model("OrderDetail", OrderDetailSchema);

//Export OrderDetail Model
module.exports = { OrderDetailModel };

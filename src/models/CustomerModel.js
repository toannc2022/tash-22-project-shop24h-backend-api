// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Customer Schema MongoDB
const CustomerSchema = new Schema({
    Id: Schema.Types.ObjectId, // Trường Id có kiểu dữ liệu ObjectID
    FullName: {
        type: String,
        required: true,
        unique: true
    },
    PhoneNumber: {
        type: String,
        required: true,
        unique: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true,

    },
    Address: {
        type: String,
        required: true,

    },
    City: {
        type: String,
        required: true,

    },
    Country: {
        type: String,
        required: false
    },
    ShoppingCard: {
        type: Array,
        required: false
    },
    WishList: {
        type: Array,
        required: false
    },
    BuyAgain: {
        type: Array,
        required: false
    },
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
})

//Tạo Customer Model
const CustomerModel = mongoose.model("Customer", CustomerSchema);

//Export Customer Model
module.exports = { CustomerModel };

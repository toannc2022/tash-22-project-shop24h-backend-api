// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo OrderByUser Schema MongoDB
const OrderByUserSchema = new Schema({
    Id: Schema.Types.ObjectId, // Trường Id có kiểu dữ liệu ObjectID
    SoDienThoai: {
        type: String,
        required: true,
    },
    DiaChi: {
        type: String,
        required: true,
    },
    SanPham: {
        type: Object,
        required: true,
    },
    TongSoTien: {
        type: String,
        required: true,
    },
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
})

//Tạo OrderByUser Model
const OrderByUserModel = mongoose.model("OrderByUser", OrderByUserSchema);

//Export OrderByUser Model
module.exports = { OrderByUserModel };

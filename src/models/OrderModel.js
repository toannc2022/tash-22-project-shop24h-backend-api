// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Order Schema MongoDB
const OrderSchema = new Schema({
    Id: Schema.Types.ObjectId, // Trường Id có kiểu dữ liệu ObjectID
    Customer: {
        CustomerID: {
            type: String,
            required: true,
            unique: true
        },
        RefCustomer: {
            type: String,
            required: true,
        }
    },
    OrderDate: {
        type: Date,
        required: false,
        default: Date.now()

    },
    RequiredDate: {
        type: Date,
        required: false,
        default: Date.now()
    },
    ShippedDate: {
        type: Date,
        required: false,
        default: Date.now()
    },
    Note: {
        type: Number,
        required: false
    },
    Status: {
        type: Number,
        required: false,
        default: 0
    },
    TimeCreated: {
        type: Date,
        required: false,
        default: Date.now()
    },
    TimeUpdated: {
        type: Date,
        required: false,
        default: Date.now()
    }
})

//Tạo Order Model
const OrderModel = mongoose.model("Order", OrderSchema);

//Export Order Model
module.exports = { OrderModel };

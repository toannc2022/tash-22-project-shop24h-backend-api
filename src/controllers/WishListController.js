// Import thư viện mongoose JS
const mongoose = require('mongoose');

// Import Course Model
const { CourseModel } = require('../models/CourseModel');
// Import WishList Model
const { WishListModel } = require('../models/WishListModel');

// Create WishList
function createWishList(req, res) {
    // Khởi tạo một đối tượng WishListModel  mới truyền các tham số tương ứng từ request body vào
    const WishList = new WishListModel({
        // Thuộc tính _id - Random một ObjectID
        _id: mongoose.Types.ObjectId(),
        Name: req.body.Name,
        Type: req.body.Type,
        ImageUrl: req.body.ImageUrl,
        BuyPrice: req.body.BuyPrice,
        PromotionPrice: req.body.PromotionPrice,
        Description: req.body.Description,
        Quantity: req.body.Quantity,
        TimeCreated: req.body.TimeCreated,
        TimeUpdated: req.body.TimeUpdated
    });

    // Gọi hàm WishList save - là 1 promise (Tiến trình bất đồng bộ)
    WishList.save()
        // Sau khi tạo WishList thành công
        .then(function (newWishList) {
            // Lấy courseId từ params URL (Khác với Query URL (sau ?))
            var courseId = req.params.courseId;

            // Gọi hàm CourseModel .findOneAndUpdate truyền vào điều kiện (_id = ?) và thêm _id của WishList mới tạo vào mảng WishLists
            return CourseModel.findOneAndUpdate({ _id: courseId }, { $push: { WishList: newWishList._id } }, { new: true });
        })
        // Sau khi update course thành công trả ra status 200 - Success
        .then((updatedCourse) => {
            return res.status(200).json({
                success: true,
                message: 'New WishList created successfully on Course',
                Course: updatedCourse,
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((error) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// Get all WishList of course 
function getAllWishListOfCourse(req, res) {
    // Lấy courseId từ params URL (Khác với Query URL (sau ?))
    const courseId = req.params.courseId;

    // Gọi hàm .findById để tìm kiếm course dựa vào ID
    CourseModel.findById(courseId)
        // Lấy chi tiết WishList dựa vào ánh xạ _id của từng phần tử trong trường WishLists
        .populate({ path: 'WishList', select: 'Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated' })

        // Nếu thành công trả ra status 200 - Success
        .then((singleCourse) => {
            res.status(200).json({
                success: true,
                message: `More WishLists on ${singleCourse.title}`,
                WishLists: singleCourse
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This course does not exist',
                error: err.message,
            });
        });
}

// Get all WishList
function getAllWishList(req, res) {
    WishListModel.find()
        .select('_id Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated')
        .then((allWishList) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all WishList',
                WishList: allWishList,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get single WishList
function getSingleWishList(req, res) {
    const id = req.params.WishListId;

    WishListModel.findById(id)
        .then((singleWishList) => {
            res.status(200).json({
                success: true,
                message: `Get data on WishList`,
                WishList: singleWishList,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This WishList does not exist',
                error: err.message,
            });
        });
}

// update WishList
function updateWishList(req, res) {
    const WishListId = req.params.WishListId;
    const updateObject = req.body;
    WishListModel.findByIdAndUpdate(WishListId, updateObject)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'WishList is updated',
                updateWishList: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a WishList
function deleteWishList(req, res) {
    const id = req.params.WishListId;

    WishListModel.findByIdAndRemove(id)
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}

module.exports = { createWishList, getAllWishListOfCourse, getAllWishList, updateWishList, getSingleWishList, deleteWishList }

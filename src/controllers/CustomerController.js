const mongoose = require("mongoose");

const { CustomerModel } = require("../models/CustomerModel");

function CreateCustomer(request, response) {
    const Customer = new CustomerModel({
        Id: mongoose.Types.ObjectId(),
        FullName: request.body.FullName,
        PhoneNumber: request.body.PhoneNumber,
        Email: request.body.Email,
        Password: request.body.Password,
        Address: request.body.Address,
        City: request.body.City,
        Country: request.body.Country,
        ShoppingCard: request.body.ShoppingCard,
        WishList: request.body.WishList,
        BuyAgain: request.body.BuyAgain,
        TimeCreated: request.body.TimeCreated,
        TimeUpdated: request.body.TimeUpdated
    });

    Customer.save()
        .then((newCustomer) => {
            return response.status(200).json({
                message: "Success",
                Customer: newCustomer
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetAllCustomer(request, response) {
    let minPrice = request.query.minPrice;
    let maxPrice = request.query.maxPrice;

    CustomerModel.find()
        .select("Id FullName PhoneNumber Email Password Address City Country ShoppingCard WishList BuyAgain TimeCreated TimeUpdated")
        .then((CustomerList) => {
            return response.status(200).json({
                message: "Success",
                Customers: CustomerList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetCustomerById(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;
    // const Password = request.params.Password;
    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        CustomerModel.find({ "Id": Id })
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        Customer: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function UpdateCustomerByID(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    const updateObject = request.body;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        CustomerModel.findOneAndUpdate({ "Id": Id }, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function DeleteCustomer(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        CustomerModel.findOneAndDelete({ "Id": Id })
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

module.exports = { CreateCustomer, GetAllCustomer, GetCustomerById, UpdateCustomerByID, DeleteCustomer }

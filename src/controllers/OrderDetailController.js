const mongoose = require("mongoose");

const { OrderDetailModel } = require("../models/OrderDetailModel");

function CreateOrderDetail(request, response) {
    const OrderDetail = new OrderDetailModel({
        Id: mongoose.Types.ObjectId(),
        Order: request.body.Order,
        Product: request.body.Product,
        Quantity: request.body.Quantity,
        PriceEach: request.body.PriceEach
    });

    OrderDetail.save()
        .then((newOrderDetail) => {
            return response.status(200).json({
                message: "Success",
                OrderDetail: newOrderDetail
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetAllOrderDetail(request, response) {
    OrderDetailModel.find()
        .select("Id Order Product Quantity PriceEach")
        .then((OrderDetailList) => {
            return response.status(200).json({
                message: "Success",
                OrderDetails: OrderDetailList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetOrderDetailById(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderDetailModel.find({ "Id": Id })
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        OrderDetail: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function UpdateOrderDetailByID(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    const updateObject = request.body;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderDetailModel.findOneAndUpdate({ "Id": Id }, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function DeleteOrderDetail(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderDetailModel.findOneAndDelete({ "Id": Id })
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

module.exports = { CreateOrderDetail, GetAllOrderDetail, GetOrderDetailById, UpdateOrderDetailByID, DeleteOrderDetail }

const mongoose = require("mongoose");

const { OrderByUserModel } = require("../models/OrderByUserModel");

function CreateOrderByUser(request, response) {
    const OrderByUser = new OrderByUserModel({
        Id: mongoose.Types.ObjectId(),
        SoDienThoai: request.body.SoDienThoai,
        DiaChi: request.body.DiaChi,
        SanPham: request.body.SanPham,
        TongSoTien: request.body.TongSoTien,
        TimeCreated: request.body.TimeCreated,
        TimeUpdated: request.body.TimeUpdated
    });

    OrderByUser.save()
        .then((newOrderByUser) => {
            return response.status(200).json({
                message: "Success",
                OrderByUser: newOrderByUser
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetAllOrderByUser(request, response) {
    OrderByUserModel.find()
        .select("Id SoDienThoai DiaChi SanPham TongSoTien TimeCreated TimeUpdated")
        .then((OrderByUserList) => {
            return response.status(200).json({
                message: "Success",
                OrderByUsers: OrderByUserList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetOrderByUserById(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderByUserModel.find({ "Id": Id })
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        OrderByUser: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function UpdateOrderByUserByID(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    const updateObject = request.body;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderByUserModel.findOneAndUpdate({ "Id": Id }, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function DeleteOrderByUser(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        OrderByUserModel.findOneAndDelete({ "Id": Id })
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

module.exports = { CreateOrderByUser, GetAllOrderByUser, GetOrderByUserById, UpdateOrderByUserByID, DeleteOrderByUser }

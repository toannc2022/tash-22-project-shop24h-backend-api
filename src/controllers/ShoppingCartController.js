const mongoose = require("mongoose");

const { ShoppingCartModel } = require("../models/ShoppingCartModel");

function CreateShoppingCart(request, response) {
    const ShoppingCart = new ShoppingCartModel({
        Id: mongoose.Types.ObjectId(),
        Name: request.body.Name,
        Type: request.body.Type,
        ImageUrl: request.body.ImageUrl,
        BuyPrice: request.body.BuyPrice,
        PromotionPrice: request.body.PromotionPrice,
        Description: request.body.Description,
        Quantity: request.body.Quantity,
        TimeCreated: request.body.TimeCreated,
        TimeUpdated: request.body.TimeUpdated
    });

    ShoppingCart.save()
        .then((newShoppingCart) => {
            return response.status(200).json({
                message: "Success",
                ShoppingCart: newShoppingCart
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetAllShoppingCart(request, response) {
    ShoppingCartModel.find()
        .select("Id Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated")
        .then((ShoppingCartList) => {
            return response.status(200).json({
                message: "Success",
                ShoppingCarts: ShoppingCartList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetShoppingCartById(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ShoppingCartModel.find({ "Id": Id })
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        ShoppingCart: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function UpdateShoppingCartByID(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    const updateObject = request.body;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ShoppingCartModel.findOneAndUpdate({ "Id": Id }, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function DeleteShoppingCart(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ShoppingCartModel.findOneAndDelete({ "Id": Id })
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

module.exports = { CreateShoppingCart, GetAllShoppingCart, GetShoppingCartById, UpdateShoppingCartByID, DeleteShoppingCart }

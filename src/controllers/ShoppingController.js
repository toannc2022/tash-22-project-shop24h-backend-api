// Import thư viện mongoose JS
const mongoose = require('mongoose');

// Import Course Model
const { CourseModel } = require('../models/CourseModel');
// Import Shopping Model
const { ShoppingModel } = require('../models/ShoppingModel');

// Create Shopping
function createShopping(req, res) {
    // Khởi tạo một đối tượng ShoppingModel  mới truyền các tham số tương ứng từ request body vào
    const Shopping = new ShoppingModel({
        // Thuộc tính _id - Random một ObjectID
        _id: mongoose.Types.ObjectId(),
        Name: req.body.Name,
        Type: req.body.Type,
        ImageUrl: req.body.ImageUrl,
        BuyPrice: req.body.BuyPrice,
        PromotionPrice: req.body.PromotionPrice,
        Description: req.body.Description,
        Quantity: req.body.Quantity,
        TimeCreated: req.body.TimeCreated,
        TimeUpdated: req.body.TimeUpdated
    });

    // Gọi hàm Shopping save - là 1 promise (Tiến trình bất đồng bộ)
    Shopping.save()
        // Sau khi tạo Shopping thành công
        .then(function (newShopping) {
            // Lấy courseId từ params URL (Khác với Query URL (sau ?))
            var courseId = req.params.courseId;

            // Gọi hàm CourseModel .findOneAndUpdate truyền vào điều kiện (_id = ?) và thêm _id của Shopping mới tạo vào mảng Shoppings
            return CourseModel.findOneAndUpdate({ _id: courseId }, { $push: { ShoppingCard: newShopping._id } }, { new: true });
        })
        // Sau khi update course thành công trả ra status 200 - Success
        .then((updatedCourse) => {
            return res.status(200).json({
                success: true,
                message: 'New Shopping created successfully on Course',
                Course: updatedCourse,
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((error) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// Get all Shopping of course 
function getAllShoppingOfCourse(req, res) {
    // Lấy courseId từ params URL (Khác với Query URL (sau ?))
    const courseId = req.params.courseId;

    // Gọi hàm .findById để tìm kiếm course dựa vào ID
    CourseModel.findById(courseId)
        // Lấy chi tiết Shopping dựa vào ánh xạ _id của từng phần tử trong trường Shoppings
        .populate({ path: 'ShoppingCard', select:'Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated' })

        // Nếu thành công trả ra status 200 - Success
        .then((singleCourse) => {
            res.status(200).json({
                success: true,
                message: `More Shoppings on ${singleCourse.title}`,
                Shoppings: singleCourse
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This course does not exist',
                error: err.message,
            });
        });
}

// Get all Shopping
function getAllShopping(req, res) {
    ShoppingModel.find()
        .select('_id Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated')
        .then((allShopping) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all Shopping',
                Shopping: allShopping,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get single Shopping
function getSingleShopping(req, res) {
    const id = req.params.ShoppingId;

    ShoppingModel.findById(id)
        .then((singleShopping) => {
            res.status(200).json({
                success: true,
                message: `Get data on Shopping`,
                Shopping: singleShopping,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This Shopping does not exist',
                error: err.message,
            });
        });
}

// update Shopping
function updateShopping(req, res) {
    const ShoppingId = req.params.ShoppingId;
    const updateObject = req.body;
    ShoppingModel.findByIdAndUpdate(ShoppingId, updateObject)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Shopping is updated',
                updateShopping: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a Shopping
function deleteShopping(req, res) {
    const id = req.params.ShoppingId;

    ShoppingModel.findByIdAndRemove(id)
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}

module.exports = { createShopping, getAllShoppingOfCourse, getAllShopping, updateShopping, getSingleShopping, deleteShopping }

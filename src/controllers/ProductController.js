const mongoose = require("mongoose");

const { ProductModel } = require("../models/ProductModel");

function CreateProduct(request, response) {
    const Product = new ProductModel({
        Id: mongoose.Types.ObjectId(),
        Name: request.body.Name,
        Type: request.body.Type,
        ImageUrl: request.body.ImageUrl,
        BuyPrice: request.body.BuyPrice,
        PromotionPrice: request.body.PromotionPrice,
        Description: request.body.Description,
        Quantity: request.body.Quantity,
        TimeCreated: request.body.TimeCreated,
        TimeUpdated: request.body.TimeUpdated
    });

    Product.save()
        .then((newProduct) => {
            return response.status(200).json({
                message: "Success",
                Product: newProduct
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetAllProduct(request, response) {
    ProductModel.find()
        .select("Id Name Type ImageUrl BuyPrice PromotionPrice Description Quantity TimeCreated TimeUpdated")
        .then((ProductList) => {
            return response.status(200).json({
                message: "Success",
                Products: ProductList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function GetProductById(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ProductModel.find({ "Id": Id })
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        Product: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function UpdateProductByID(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    const updateObject = request.body;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ProductModel.findOneAndUpdate({ "Id": Id }, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

function DeleteProduct(request, response) {
    // Lấy Id từ params URL
    const Id = request.params.Id;

    // Kiểm tra xem Id có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(Id)) {
        ProductModel.findOneAndDelete({ "Id": Id })
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "Id is not valid"
        })
    }
}

module.exports = { CreateProduct, GetAllProduct, GetProductById, UpdateProductByID, DeleteProduct }

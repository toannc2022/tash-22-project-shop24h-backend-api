const express = require("express");

const router = express.Router();

const { createCourse, getAllCourse, getCourseDetail, updateCourseByID, deleteCourse } = require("../controllers/CourseController");
const { createShopping, getAllShoppingOfCourse } = require("../controllers/ShoppingController");
const { createWishList, getAllWishListOfCourse } = require("../controllers/WishListController");



router.post("/", createCourse);
router.get("/", getAllCourse);

router.get("/:courseId", getCourseDetail);
router.put("/:courseId", updateCourseByID);
router.delete("/:courseId", deleteCourse);

// /courses/:courseId/Shoppings - Create Shopping
router.post('/:courseId/Shoppings', createShopping);
// /courses/:courseId/Shoppings - Get all Shopping of course
router.get('/:courseId/Shoppings', getAllShoppingOfCourse);

// /courses/:courseId/Shoppings - Create Shopping
router.post('/:courseId/wishlist', createWishList);
// /courses/:courseId/Shoppings - Get all Shopping of course
router.get('/:courseId/wishlist', getAllWishListOfCourse);

module.exports = router;

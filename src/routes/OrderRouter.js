const express = require("express");

const router = express.Router();

const { CreateOrder, GetAllOrder, GetOrderById, UpdateOrderByID, DeleteOrder } = require("../controllers/OrderController");

router.post("/", CreateOrder);
router.get("/", GetAllOrder);

router.get("/:Id", GetOrderById);
router.put("/:Id", UpdateOrderByID);
router.delete("/:Id", DeleteOrder);

module.exports = router;

const express = require("express");

const router = express.Router();

const { CreateCustomer, GetAllCustomer, GetCustomerById, UpdateCustomerByID, DeleteCustomer } = require("../controllers/CustomerController");

router.post("/", CreateCustomer);
router.get("/", GetAllCustomer);

router.get("/:Id", GetCustomerById);
router.put("/:Id", UpdateCustomerByID);
router.delete("/:Id", DeleteCustomer);

module.exports = router;

const express = require("express");

const router = express.Router();

const { CreateShoppingCart, GetAllShoppingCart, GetShoppingCartById, UpdateShoppingCartByID, DeleteShoppingCart } = require("../controllers/ShoppingCartController");

router.post("/", CreateShoppingCart);
router.get("/", GetAllShoppingCart);

router.get("/:Id", GetShoppingCartById);
router.put("/:Id", UpdateShoppingCartByID);
router.delete("/:Id", DeleteShoppingCart);

module.exports = router;

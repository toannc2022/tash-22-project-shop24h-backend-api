const express = require("express");

const router = express.Router();

const { CreateOrderByUser, GetAllOrderByUser, GetOrderByUserById, UpdateOrderByUserByID, DeleteOrderByUser } = require("../controllers/OrderByUserController");

router.post("/", CreateOrderByUser);
router.get("/", GetAllOrderByUser);

router.get("/:Id", GetOrderByUserById);
router.put("/:Id", UpdateOrderByUserByID);
router.delete("/:Id", DeleteOrderByUser);

module.exports = router;

const express = require("express");

const router = express.Router();

const { CreateOrderDetail, GetAllOrderDetail, GetOrderDetailById, UpdateOrderDetailByID, DeleteOrderDetail } = require("../controllers/OrderDetailController");

router.post("/", CreateOrderDetail);
router.get("/", GetAllOrderDetail);

router.get("/:Id", GetOrderDetailById);
router.put("/:Id", UpdateOrderDetailByID);
router.delete("/:Id", DeleteOrderDetail);

module.exports = router;

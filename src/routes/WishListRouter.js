// Import thư viện Express
const express = require('express');

// Import các hàm của lớp controller
const { getAllWishList, deleteWishList, getSingleWishList, updateWishList } = require("../controllers/WishListController");

// Khai báo exporess router
const router = express.Router();

// Khai báo các router dạng "/WishLists" + url bên dưới sẽ gọi đến các hàm tương ứng

// /WishLists - Get All WishLists
router.get('/', getAllWishList);
// /WishLists/:WishListId - Get WishList By ID
router.get('/:WishListId', getSingleWishList);
// /WishLists/:WishListId - Update WishList By ID
router.put('/:WishListId', updateWishList);
// /WishLists/:WishListId - Delete WishList By ID
router.delete('/:WishListId', deleteWishList);

// Export router dưới dạng module để sử dụng
module.exports = router;

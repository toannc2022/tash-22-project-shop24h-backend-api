const express = require("express");

const router = express.Router();

const { CreateProduct, GetAllProduct, GetProductById, UpdateProductByID, DeleteProduct } = require("../controllers/ProductController");

router.post("/", CreateProduct);
router.get("/", GetAllProduct);

router.get("/:Id", GetProductById);
router.put("/:Id", UpdateProductByID);
router.delete("/:Id", DeleteProduct);

module.exports = router;

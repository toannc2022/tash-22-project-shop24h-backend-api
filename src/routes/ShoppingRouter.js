// Import thư viện Express
const express = require('express');

// Import các hàm của lớp controller
const { getAllShopping, deleteShopping, getSingleShopping, updateShopping } = require("../controllers/ShoppingController");

// Khai báo exporess router
const router = express.Router();

// Khai báo các router dạng "/Shoppings" + url bên dưới sẽ gọi đến các hàm tương ứng

// /Shoppings - Get All Shoppings
router.get('/', getAllShopping);
// /Shoppings/:ShoppingId - Get Shopping By ID
router.get('/:ShoppingId', getSingleShopping);
// /Shoppings/:ShoppingId - Update Shopping By ID
router.put('/:ShoppingId', updateShopping);
// /Shoppings/:ShoppingId - Delete Shopping By ID
router.delete('/:ShoppingId', deleteShopping);

// Export router dưới dạng module để sử dụng
module.exports = router;

const express = require("express"); // Tương ứng với import express from 'express'
const mongoose = require("mongoose"); // Tương ứng với import mongoose from 'mongoose'

const ProductRouter = require("./src/routes/ProductRouter");
const CustomerRouter = require("./src/routes/CustomerRouter");
const OrderRouter = require("./src/routes/OrderRouter");
const OrderDetailRouter = require("./src/routes/OrderDetailRouter");

const courseRouter = require("./src/routes/CourseRouter");
const ShoppingRouter = require('./src/routes/ShoppingRouter');

const ShoppingCartRouter = require('./src/routes/ShoppingCartRouter');

const OrderByUserRouter = require('./src/routes/OrderByUserRouter');
const WishListRouter = require('./src/routes/WishListRouter');


const app = express();

// Khai báo body lấy tiếng Việt
app.use(express.urlencoded({
    extended: true
}))
//Khai báo body dạng JSON
app.use(express.json());

const port = 8080;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// Kết nối với MongoDB
async function connectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h_NodeJS");
}

//Thực thi kết nối
connectMongoDB()
    .then(() => console.log("Connect MongoDB Successfully"))
    .catch(err => console.log(err))

app.get("/", (request, response) => {
    response.json({
        message: "CRUD API"
    })
})

app.use("/products", ProductRouter);
app.use("/customers", CustomerRouter);
app.use("/orders", OrderRouter);
app.use("/orderDetails", OrderDetailRouter);


app.use("/courses", courseRouter);
app.use("/Shoppings", ShoppingRouter);

app.use("/shoppingcarts", ShoppingCartRouter);

app.use("/orderbyusers", OrderByUserRouter);
app.use("/wishlist", WishListRouter);



app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})
